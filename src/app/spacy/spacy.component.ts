import { Component, Input, OnInit } from '@angular/core';
import { MyServiceService } from '../spacy.service';
import { ISpaCy } from '../spacy';
import { ITextOutput } from '../textouput';



@Component({
  selector: 'spacy',
  templateUrl: './spacy.component.html',
  styleUrls: ['./spacy.component.css']
})
export class SpacyComponent implements OnInit{
  constructor(private spacyService: MyServiceService){

   
    }
    public spacy : ISpaCy = {textInput:'' , model:'en'}
    public textOutput: ITextOutput = {arcs: [], words: []}

  ngOnInit() { 
  }
spacyGetText(): void {
  this.spacyService.getText(this.spacy.textInput, this.spacy.model).subscribe((data) =>{
this.textOutput.arcs = data.arcs.slice();
this.textOutput.words = data.words.slice(); 

  })
    
}
  

  

  
}
