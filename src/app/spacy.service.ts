import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ITextOutput } from './textouput';



@Injectable({
  providedIn: 'root'
})
export class MyServiceService {
  private _url: string = "https://spacy.frag.jetzt/dep"
 

  constructor(private http: HttpClient) { }
  getText(text: string, model: string): Observable<any>{
    return this.http.post<any>(this._url, {text: text, model: model});
  }
  

}
